let clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
let clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const mergedList = [...new Set([...clients1, ...clients2])]

console.log(mergedList)

//////// 2 /////////

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

    let charactersShortInfo = characters.map(function (item){
        const {name,lastName,age} = item;
        return {name,lastName,age};
    })
    console.log(charactersShortInfo)

//////// 3 /////////

const user1 = {
    name: "John",
    years: 30
};

const {name: name, years: age, isAdmin: isAdmin = false} = user1;

console.log(`The name of the user is ${name}, he is ${age} years old. Is it correct that he's an admin? - ${isAdmin}`)

//////// 4 /////////

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const actualInfo = {...satoshi2018, ...satoshi2019, ...satoshi2020};

console.log(actualInfo)

//////// 5 /////////

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const updatedList = [...books, {...bookToAdd}];
console.log(updatedList);

//////// 6 /////////

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}
const employeeExtended = {
    age: 49,
    salary: '16000 UAH'
}

const employeeInfo = {...employee, ...employeeExtended};
console.log(employeeInfo)

//////// 7 /////////

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
