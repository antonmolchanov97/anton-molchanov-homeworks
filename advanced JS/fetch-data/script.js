let url = 'https://ajax.test-danit.com/api/swapi/films';

fetch(url)
    .then(response => response.json())
    .then(movies => {
        movies.forEach(item => {
            document.body.insertAdjacentHTML('beforeend', `
                <div class="episode${item.id}"
                <p><b>Номер эпизода:</b> ${item.episodeId}</p>
                <p><b>Название фильма:</b> ${item.name}</p>
                <p><b>Краткое содержание:</b> ${item.openingCrawl}</p>
                <ul class="characters-list${item.id}">
                </ul>
                <hr>
                </div>           
                `)
            let urls = item.characters;

            let requests = urls.map(url => fetch(url).then(respnose => respnose.json()));
            Promise.all(requests)
                .then(data => data.forEach(person =>
                    document.querySelector(`.characters-list${item.id}`).append(person.name)))
                    // console.log(person.name)))


        })
    })
