/// try...catch уместно использовать тогда, когда выполнение кода может зависить от внешних обстоятельств, а не от
//кода, который написал программист. В данном случае, выполнение программы не зависит ни от соединения с интернетом,
//ни от сервера, ни от дополнительных плагинов.
//Так как ничего из перечисленного в этом дз нет, я искренне не понимаю, куда и, главное, зачем тут задействовать try...catch


const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const booksContainer = document.querySelector('#root');
const ul = document.createElement('ul');
booksContainer.append(ul);

let booksOut = books.map(function (item){
    const {author,name,price} = item;
    if (author && name && price){
        return ul.insertAdjacentHTML('beforeend',`<li>Автор книги: ${author}</li><li>Название книги:${name}</li><li>Цена книги:${price}</li><br>`)
    }
})

function booksToEdit(bookItem){
    const {author,name,price} = bookItem;
    if (!author){
        throw new Error(`Please add an author to the book ${name} with a price ${price}`)
    } else if(!name){
        throw new Error(`Please add a book name to the book of ${author} with the price ${price}`)
    } else if(!price){
        throw new Error(`Please add a price to the book ${name}`)
    }
}

for (let i = 0; i < books.length; i++){
    try{
        booksToEdit(books[i])
    }catch (e){
        console.log(e)
    }
}


