const container = document.createElement('div');
container.classList.add('container')
document.body.append(container)
const ipButton = document.createElement('div');
ipButton.textContent = 'Вычислить по IP'
ipButton.classList.add('ipButton')
container.append(ipButton)

const userInfoContainer = document.createElement('ul')
container.append(userInfoContainer)


const url1 = 'https://api.ipify.org/?format=json';
const url2 = 'http://ip-api.com/'

ipButton.addEventListener('click', async () => {
    try {
        userInfoContainer.textContent = ''
        const requestIp = await fetch(url1)
        const ipObject = await requestIp.json()
        console.log(ipObject.ip)

        const userInfo = await fetch(`${url2}/json/${ipObject.ip}?fields=continent,country,region,city,district`)
        const data = await userInfo.json();
        console.log(data)


        for (let param in data){
            let li = document.createElement('li')
            li.classList.add('info-item')
            userInfoContainer.append(li)
            li.textContent = `${param}: ${data[param]}`
            console.log(`${data[param]}`)
        }

    } catch (e) {
        console.error(e)
    }
})
