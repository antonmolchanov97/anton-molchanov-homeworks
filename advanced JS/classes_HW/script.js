class Employee {
    constructor(name,age,salary) {
        this._name = name;
        this._age = age;
        this._salary = `$${salary} USD`;
    }
    get name(){
        return this._name
    }
    set name(newName){
        this._name = newName;
    }
    get age(){
        return this._age;
    }
    set age(newAge){
        this._age = newAge;
    }
    get salary(){
        return this._salary
    }
    set salary(newSalary){
        this._salary = `$${newSalary} USD`
    }
}


class Programmer extends Employee {
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this._lang = lang;
    }
    get programmersalary(){
        return '$' + parseInt(this._salary.match(/\d+/) * 3) + ' USD';
    }
}

const antonEmployee = new Employee('Anton',23,1600);
console.log(antonEmployee)
antonEmployee.name = 'Tony';
antonEmployee.age = 24;
antonEmployee.salary = 2500
console.log(antonEmployee)


const programmer = new Programmer('Kostia',16,3000,'Eng,German')
console.log(programmer)
programmer.name = 'Konstantin';
programmer.age = 17;
programmer.salary = 3500;
programmer.lang = 'JavaScript, C++, C#, Python'
console.log(programmer)
