const workImages = document.querySelectorAll('.img-wrapper');
console.log(workImages);

     workImages.forEach(item => item.addEventListener('click', function(event){
     if(!event.target.classList.contains('work-images')){
        event.target.parentElement.insertAdjacentHTML('afterbegin', '<div id="replacement" class="replacementblock"><div class="icons"><a href="#" class="link icon-link link-chain"><svg class="chain-svg" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/></svg></a><a href="#" class="link icon-link link-square"></a></div><a href="#" class="link"><h3 class="icon-headline">creative design</h3></a><a href="#" class="link"><h4 class="icon-subheading">Web Design</h4></a></div>')
        event.target.removeEventListener();
   }
}))

        //  document.querySelectorAll('.image-item').forEach(item => {
        //      item.classList.remove('hidden')
        //      if(!item.classList.contains(tabClass)){
        //          item.classList.add('hidden')
        //          console.log(event.target.dataset)
        //      }
        // })

        const workImages = document.querySelectorAll('.img-wrapper');

        workImages.forEach(item => item.addEventListener('mouseenter', function(event){
        if(event.target.classList.contains('img-wrapper')){
           event.target.insertAdjacentHTML('beforeend', `<div id="replacement" class="replacementblock">
           <div class="icons"><a href="#" class="link icon-link link-chain">
           <svg class="chain-svg" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/></svg>
           </a><a href="#" class="link icon-link link-square"></a></div><a href="#" class="link"><h3 class="icon-headline">creative design</h3></a><a href="#" class="link"><h4 class="icon-subheading">${event.target.dataset.tab[0].toUpperCase() + event.target.dataset.tab.slice(1).replace('_', " ")}</h4></a></div>`)
       }
   
      workImages.forEach(item => item.addEventListener('mouseleave', event=>{
       document.querySelector('#replacement').remove()
      }))
   }))

   // let workTabsBtn = document.querySelectorAll('.work-tab-item');

// let images = [
//     {
//         category: "tab_graphic_design",
//         url: [
//             "./IMG/graphic design/graphic-design1.jpg",
//             "./IMG/graphic design/graphic-design2.jpg",
//             "./IMG/graphic design/graphic-design3.jpg",
//             "./IMG/graphic design/graphic-design4.jpg",
//             "./IMG/graphic design/graphic-design5.jpg",
//             "./IMG/graphic design/graphic-design6.jpg",
//             "./IMG/graphic design/graphic-design7.jpg",
//             "./IMG/graphic design/graphic-design8.jpg"
//         ]
//     },
//     {
//         category: "tab_web_design",
//         url: [
//             "./IMG/graphic designgraphic-design8.jpg",
//             "./IMG/web design/web-design1.jpg",
//             "./IMG/web design/web-design2.jpg",
//             "./IMG/web design/web-design3.jpg",
//             "./IMG/web design/web-design4.jpg",
//             "./IMG/web design/web-design5.jpg",
//             "./IMG/web design/web-design6.jpg",
//             "./IMG/web design/web-design7.jpg"
//         ]
//     },
//     {
//         category: "tab_landing_pages",
//         url: [
//             "./IMG/landing page/landing-page1.jpg",
//             "./IMG/landing page/landing-page2.jpg",
//             "./IMG/landing page/landing-page3.jpg",
//             "./IMG/graphic design/graphic-design8.jpg",
//             "./IMG/landing page/landing-page5.jpg",
//             "./IMG/landing page/landing-page6.jpg",
//             "./IMG/landing page/landing-page7.jpg",
//             "./IMG/landing page/landing-page4.jpg",
//         ]
//     },
//     {
//         category: "tab_wordpress",
//         url: [
//             "./IMG/wordpress/wordpress1.jpg",
//             "./IMG/wordpress/wordpress2.jpg",
//             "./IMG/wordpress/wordpress3.jpg",
//             "./IMG/wordpress/wordpress4.jpg",
//             "./IMG/wordpress/wordpress5.jpg",
//             "./IMG/wordpress/wordpress6.jpg",
//             "./IMG/wordpress/wordpress7.jpg",
//             "./IMG/wordpress/wordpress8.jpg"
//         ]
//     },
//     // {
//     //     category: "tab_all",
//     //     url: [
//     //         "./IMG/graphic design/graphic-design1.jpg",
//     //         "./IMG/graphic design/graphic-design2.jpg",
//     //         "./IMG/graphic design/graphic-design3.jpg",
//     //         "./IMG/web design/web-design1.jpg",
//     //         "./IMG/web design/web-design2.jpg",
//     //         "./IMG/web design/web-design3.jpg",
//     //         "./IMG/landing page/landing-page1.jpg",
//     //         "./IMG/landing page/landing-page2.jpg",
//     //         "./IMG/landing page/landing-page3.jpg",
//     //         "./IMG/wordpress/wordpress1.jpg",
//     //         "./IMG/wordpress/wordpress2.jpg",
//     //         "./IMG/wordpress/wordpress3.jpg",
//     //     ]
//     // }
// ]

// // const newArr = images.map(item => item.url.slice(0, 3));
// // let b;

// // function flatten (arr, result = []) {
// //     for (let i = 0, length = arr.length; i < length; i++) {
// //       const value = arr[i];
// //       if (Array.isArray(value)) {
// //         flatten(value, result);
// //       } else {
// //         result.push(value);
// //       }
// //     }
// //     return result;
// //   };

// // console.log(newArr);
// // flatten(newArr, b).forEach(item => {
    
// // })

// let workImagesWrapper = document.querySelector('.work-images');
// let imageWrapper;
// let newImg;
// images.forEach(obj => {

// if(obj.category === 'tab_all'){
//     obj.url.forEach(link => {
//         // newImg = `<img class="${tabClass}" src="${link}">`;
//         newImg = `<img class="tab_all" src="${link}">`;
//         imageWrapper = document.createElement('div');
//         imageWrapper.classList.add('image-wrapper')
//         workImagesWrapper.append(imageWrapper);
//         imageWrapper.insertAdjacentHTML('afterbegin', newImg);
//     })
// }
// })

// workTabsBtn.forEach(function(item){

//     item.addEventListener('click', function(event){
//         workTabsBtn.forEach(function(item){
//             item.classList.remove('work-active-tab')
//     })
//         event.target.classList.add('work-active-tab');
//         images.forEach(obj => {

//         if(event.target.dataset.tab === obj.category){ // && event.target.dataset.tab !== 'tab_all'
//             document.querySelectorAll('.image-wrapper').forEach(wrapper => wrapper.remove());
//             obj.url.forEach(link => {
//                 newImg = `<img class="${obj.category}" src="${link}">`;
//                 imageWrapper = document.createElement('div');
//                 imageWrapper.classList.add('image-wrapper', `${obj.category}`);
//                 workImagesWrapper.append(imageWrapper);
//                 imageWrapper.insertAdjacentHTML('afterbegin', newImg);
//             })
//         }
//     })
//   })
// })


// function hoverImages(){
//     workImagesWrapper.addEventListener('mouseover',()=>{

//     let imageWrapper = document.querySelectorAll('.image-wrapper');

//     imageWrapper.forEach(item => item.addEventListener('mouseenter', function(event){
//            event.target.insertAdjacentHTML('beforeend', `<div id="replacement" class="replacementblock">
//            <div class="icons"><a href="#" class="link icon-link link-chain">
//            <svg class="chain-svg" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/></svg>
//            </a><a href="#" class="link icon-link link-square"></a></div><a href="#" class="link"><h3 class="icon-headline">creative design</h3></a><a href="#" class="link"><h4 class="icon-subheading">${event.target.classList}</h4></a></div>`)
//         }))
//         imageWrapper.forEach(item => item.addEventListener('mouseleave', event=>{
//         document.querySelector('#replacement').remove()
//         }))
//     })
// }
// hoverImages()


// let loader = document.querySelector('.loader')
// let loadButton = document.querySelector('.load-more-button');
// loadButton.addEventListener('click', (event)=> {
//     event.preventDefault();
//     loader.classList.remove('hidden-loader')
//     event.target.parentNode.replaceChild(document.querySelector('.loader'), loadButton);
//     setTimeout(function(){
//         loader.classList.add('hidden-loader')
//     },2000)
// })


 

// const hover = {
// 	webDesign: '<div class="hover"><p>web design</p></div>',
// 	wordpress: '<div class="hover"><p>wordpress/p></div>',
// 	landing: '<div class="hover"><p>landing</p></div>',
// 	wordpress: '',
// };
// const container = document.querySelector('.container');
// for (let i = 0; i <= 3; i++) {
// 	container.insertAdjacentHTML(
// 		'afterbegin',`<div data-content="graphic"><img src="./IMG/graphic design/graphic-design${i}.jpg"></div>`);
// 	container.insertAdjacentHTML(
//         'afterbegin', `<div data-content="desing">${hover.webDesign}<img src="./IMG/wordpress/wordpress${i}.jpg"></div>`);
// 	container.insertAdjacentHTML(
// 		'afterbegin',`<div data-content="graphic"><img src="./IMG/graphic design/graphic-design${i}.jpg"></div>`);
// 	container.insertAdjacentHTML(
// 		'afterbegin',`<div data-content="graphic"><img src="./IMG/graphic design/graphic-design${i}.jpg"></div>`);
// }
// // LOAD MORE
// for (let i = 4; i <= 6; i++) {
// 	container.append(`<div><img src="./IMG/graphic design/graphic-design${i}.jpg"></div>`);
// 	container.append(`<div>${hover.webDesign}<img src="../img/worpres${i}.jpg"></div>`);
// 	container.append(`<div><img src="../img/graphic-design${i}.jpg"></div>`);
// 	container.append(`<div><img src="../img/graphic-design${i}.jpg"></div>`);
// }
// if (event.target.dataset.title === item.dataset.content) {
    //     item.style.display = 'block';
    // } else if (event.target.dataset.title === 'All') {
    //     item.style.display = 'block';
    // } else {
    //     item.style.display = 'none';
    // }   

 
// 

$(document).ready(function () {
   $('.item-masonry').hover(
       function(){
           $(this).find('.grid-hover').css('display', 'flex');
       },
       function(){
           $(this).find('.grid-hover').css('display', 'none')
       }
   );
});



// $('.gallery').masonry({
//     itemSelector: '.item-masonry',
//     columnWidth: '.sizer1',
//     gutter: 10
// })

let $container = $('.gallery');

$container.imagesLoaded(function(){
   $container.masonry({
       itemSelector: '.item-masonry',
       columnWidth: '.sizer1',
       gutter: 10
   })
})

// let bestImages = document.querySelector('.gallery');
// console.log(bestImages)
// for(let i = 9; i <= 14; i++){
//     bestImages.insertAdjacentHTML('beforeend', `<div class="item-masonry sizer1"><img src="./IMG/gallery/gal${i}.png"><div class="grid-hover"><a href="#"><i class="fas fa-search"></i></a><a href=""><i class="fas fa-expand"></i></a></div></div>`)
// }


let loader2 = document.querySelector('.loader2')
let loadButton2 = document.querySelector('.load-more-button2');
loadButton2.addEventListener('click', (event)=> {
   event.preventDefault();
   loader2.classList.remove('hidden-loader2')
   event.target.parentNode.replaceChild(document.querySelector('.loader2'), loadButton2);
   setTimeout(function(){
       loader2.classList.add('hidden-loader2');
       let bestImages = document.querySelector('.gallery');
       for(let i = 9; i <= 14; i++){
           bestImages.insertAdjacentHTML('beforeend', `<div class="item-masonry sizer1"><img src="./IMG/gallery/gal${i}.png"><div class="grid-hover"><a href="#"><i class="fas fa-search"></i></a><a href=""><i class="fas fa-expand"></i></a></div></div>`);
       }
   },2000)
})

$container.imagesLoaded(function(){
   $container.masonry({
       itemSelector: '.item-masonry',
       columnWidth: '.sizer1',
       gutter: 10
   })
})