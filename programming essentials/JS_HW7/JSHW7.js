function arrayToLi(array, parent = document.body){
    const mapped = array.map(n => `<li>${n}</li>`);
    parent.insertAdjacentHTML("afterbegin",`<ul>${mapped.join('')}</ul>`)
}
arrayToLi(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])