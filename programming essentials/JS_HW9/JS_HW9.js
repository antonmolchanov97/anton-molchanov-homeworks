const gallery = document.querySelector('.tabs');

gallery.addEventListener('click', event =>{
    if(event.target.classList.contains('tabs-title')){
        document.querySelectorAll('.tabs-title').forEach(item => item.classList.remove('active'));
        document.querySelectorAll('.tab-item').forEach(item => item.classList.remove('active-tab'));
        event.target.classList.add('active');
        document.querySelector(`.${event.target.textContent.toLowerCase()}`).classList.add('active-tab')
    }
})