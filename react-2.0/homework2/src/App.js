import React from "react";
import './App.css';
import GoodsItem from "./Components/GoodsItem/GoodsItem";
import GoodsList from "./Components/GoodsList/GoodsList";
import Cart from "./Components/Cart/Cart";


export default class App extends React.Component {

    state = {
        data: [],
        favourites: [],
        cart: []
    }


    render() {
        const {isModalOpen,data,addedToFavs} = this.state
        return (
            <div className="App">
                <Cart cart={this.state.cart}
                      showActualCartState={() => this.showActualCartState()}
                />
                <GoodsList state={isModalOpen}
                           isAddedToFavs={addedToFavs}
                           data={data}
                           addToFavs={(id)=>this.addToFavs(id)}
                           removefromFavs={(id)=>this.removefromFavs(id)}
                           addToCart={(id) => this.addToCart(id)}
                           actualFavsState={this.state.favourites}
                />
            </div>
        );
    }

    componentDidMount() {
        fetch("./goods.json")
            .then(response => response.json())
            .then(data => this.setState({data}))

        let localStorageData = JSON.parse(localStorage.getItem('favourites'))
        if (!localStorageData){
            return
        }
        else{
            this.setState({favourites: localStorageData})
        }
    }


    addToFavs(id){
        this.setState({favourites: [...this.state.favourites,id]})
    }
    removefromFavs(id){
        const actualFavsState = this.state.favourites.filter(favID => favID !== id)
        this.setState({favourites: actualFavsState})
    }
    addToCart(id){
        this.setState({cart: [...this.state.cart,id]})
        localStorage.setItem('cart', JSON.stringify([...this.state.cart,id]))
    }
    showActualCartState(){
        let localStorageData = JSON.parse(localStorage.getItem('cart'))
        if (localStorageData){
            this.setState({cart: localStorageData})
        }
    }
}

