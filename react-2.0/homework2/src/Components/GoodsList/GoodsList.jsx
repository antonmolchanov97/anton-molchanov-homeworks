import React, {Component} from 'react';
import GoodsItem from "../GoodsItem/GoodsItem";
import './Goodslist.scss'
import Proptypes from 'prop-types'



class GoodsList extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const goodsList = this.props.data.map(goodItem => <GoodsItem
            key={goodItem.id}
            addToFavs={this.props.addToFavs}
            removefromFavs={this.props.removefromFavs}
            addToCart={this.props.addToCart}
            state={this.props.state}
            name={goodItem.name}
            price={goodItem.price}
            src={goodItem.src}
            id={goodItem.id}
            actualFavsState={this.props.actualFavsState}

        />)
        return (
            <div className="goodslist">
                {goodsList}
            </div>
        );
    }
}
GoodsList.propTypes = {
    state: Proptypes.bool,
    addToFavs: Proptypes.func,
    data: Proptypes.array
}

export default GoodsList;