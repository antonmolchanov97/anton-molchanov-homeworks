import React, {Component} from 'react';
import './Cart.scss'
import CartIcon from './cart-checked.svg'
import CartItems from "./CartItems/CartItems";

class Cart extends Component {
    state = {isCartOpen: false}

    openCart(){
        this.setState({isCartOpen : !this.state.isCartOpen})
        this.props.showActualCartState()
    }

    render() {

        return (
            <div>
                <img onClick={() => this.openCart()} className='cart-icon' src={CartIcon} alt=""/>
                <div className="cart-items">
                    {this.state.isCartOpen &&
                    <CartItems cart={this.props.cart}/>}
                </div>
            </div>
        );
    }
}

export default Cart;