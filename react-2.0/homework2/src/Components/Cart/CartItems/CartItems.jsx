import React, {Component} from 'react';
import './cartItems.scss'
class CartItems extends Component {
    render() {
        const listOfCartItems = this.props.cart.map(cartItem => <p>{cartItem.name}</p>)
        return (
            <div className="cart-items-wrapper">
                <p className="cart-title">Cart</p>
                {listOfCartItems}
            </div>
        );
    }
}

export default CartItems;