import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import './GoodItem.scss'
import starempty from './icons/star-empty.svg'
import starfilled from './icons/star-full.svg'

export default class GoodsItem extends React.Component {

    state = {isAddedToFavs: false, isModalOpen: false}

    componentDidMount() {
        if (this.props.actualFavsState.includes(this.props.id)){
            this.setState({isAddedToFavs: true})
        }
    }


    toggleFavsState() {
        if (!this.state.isAddedToFavs){
            this.props.addToFavs(this.props.id)
        }
        else {
            this.props.removefromFavs(this.props.id)
        }
        this.toggleDataInLocalStorage(this.props.id)
        this.setState({isAddedToFavs: !this.state.isAddedToFavs})
    }
    toggleDataInLocalStorage(id){
        const rawDataFromLocalStorage = localStorage.getItem('favourites');
        let dataFromLocalStorage = JSON.parse(rawDataFromLocalStorage);
        if (!dataFromLocalStorage){
            localStorage.setItem('favourites',JSON.stringify([id]))
            return
        }

        const idIsInArray = dataFromLocalStorage.find(item => item === id)
        if (idIsInArray){
            dataFromLocalStorage = dataFromLocalStorage.filter(item => item !== id)
            localStorage.setItem('favourites', JSON.stringify(dataFromLocalStorage))
            return;
        }
        dataFromLocalStorage.push(id)
        localStorage.setItem('favourites', JSON.stringify(dataFromLocalStorage))
    }

    toggleModal(){
        this.setState({isModalOpen: !this.state.isModalOpen})
    }

    addToCart(){
        this.props.addToCart({id: this.props.id, name: this.props.name})
        this.toggleModal()
    }




    /*
    * метод, который берет айдишник товара и добавляет его в корзину
    * метод нужно написать в onclick на ок
    * */


    render() {
        return (
            <div className='good-item'>
                <div className='item-title'>
                    <p>{this.props.name}</p>
                </div>
                <img src={this.props.src} alt=""/>
                <div className="good-pricing">
                    <div className='favIcon-wrapper' onClick={() => this.toggleFavsState()}>
                        <img className='favIcon' src={this.state.isAddedToFavs ? starfilled : starempty} alt=""/>
                    </div>
                    <p className="good-price">{this.props.price}</p>
                </div>
                <div className='button-wrapper'>
                    <Button
                        backgroundColor='#6699ff'
                        text='Add to cart'
                        onClick={() => this.toggleModal()}/>
                </div>
                {this.state.isModalOpen &&
                <Modal
                    onClick={() => this.toggleModal()}
                    header='Do you want to add this item to cart?'
                    text={this.props.name}
                    subtext={this.props.price}
                    closeButton={true}
                    id="firstModal"
                    cssClass='modal-wrapper-first modal-wrapper'
                    actions={
                        [
                            <Button
                                backgroundColor='#999999'
                                text='Ok'
                                onClick={() => this.addToCart()}
                            />,
                            <Button
                                backgroundColor='#999999'
                                text='Cancel'
                                onClick={() => this.toggleModal()}
                            />
                        ]}
                />
                }
            </div>
        )
    }
}