import React,{useState,useEffect} from "react";
import './App.css';
import Header from "./Components/Header/Header";
import AppRoutes from "./AppRoutes";
import {Context} from "./Context";
import Sidebar from "./Components/Sidebar/Sidebar";

function App() {



    const [data, setData] = useState([])
    const [favourites, setFavourites] = useState([])
    const [cart, setCart] = useState([])

    useEffect(()=>{
        fetch("./goods.json")
            .then(response => response.json())
            .then(data => setData(data))

        let localStorageData = JSON.parse(localStorage.getItem('favourites'))
        if (!localStorageData){
            return
        }
        else{
            setFavourites(localStorageData)
        }
    },[])

    useEffect(()=>{
        showActualCartState()
    },[])


    const showActualCartState = () => {
        let localStorageData = JSON.parse(localStorage.getItem('cart'))
        if (localStorageData){
            setCart(localStorageData)
        }
    }

    const addToFavs = (id) => {
        setFavourites([...favourites,id])
    }

    const removefromFavs = (id) => {
        const actualFavsState = favourites.filter(favID => favID !== id)
        setFavourites(actualFavsState)
    }

    const addToCart = (id) => {
        localStorage.setItem('cart', JSON.stringify([...cart,id]))
        setCart([...cart,id])
    }

    const removeFromCart = (id) => {
        const actualCartState = cart.filter(cartId => cartId !== id)
        localStorage.setItem('cart',JSON.stringify(actualCartState))
        setCart(actualCartState)
    }


    return (
        <div className="App">
            <Header/>
            <main className="main">
                <Context.Provider value={{data:data,favourites:favourites,cart:cart}}>

                    <AppRoutes addToFavs={addToFavs}
                               removefromFavs={removefromFavs}
                               addToCart={addToCart}
                               removeFromCart={removeFromCart}
                    />

                </Context.Provider>
                <Sidebar/>
            </main>
        </div>
    );
}

export default App;

