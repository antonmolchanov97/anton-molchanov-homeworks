import React, {useContext} from 'react';
import {Switch, Route} from 'react-router-dom'
import Cart from "./Components/Pages/Cart/Cart";
import GoodsList from "./Components/Pages/GoodsList/GoodsList";
import Favourites from "./Components/Pages/Favourites/Favourites";
import PropTypes from 'prop-types'


AppRoutes.propTypes = {
    addToFavs: PropTypes.func.isRequired,
    removefromFavs: PropTypes.func.isRequired,
    removeFromCart: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

function AppRoutes(props) {

    const {addToFavs, removefromFavs, removeFromCart, addToCart} = props;

    return (
        <Switch>
            <Route path="/cart" render={() => <Cart
                addToFavs={addToFavs}
                removefromFavs={removefromFavs}
                addToCart={addToCart}
                removeFromCart={removeFromCart}

            />}/>
            <Route path="/favourites" render={() => <Favourites
                addToFavs={addToFavs}
                removefromFavs={removefromFavs}
                addToCart={addToCart}
                removeFromCart={removeFromCart}

            />}/>
            <Route path="/" render={() => <GoodsList addToFavs={addToFavs}
                                                     removefromFavs={removefromFavs}
                                                     addToCart={addToCart}
                                                     removeFromCart={removeFromCart}

            />}/>
        </Switch>
    );
}

export default AppRoutes;