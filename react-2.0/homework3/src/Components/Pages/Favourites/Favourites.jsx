import React,{useContext} from 'react';
import GoodsItem from "../GoodsList/GoodsItem/GoodsItem";
import {Context} from "../../../Context";
import PropTypes from 'prop-types'

Favourites.propTypes = {
    addToFavs: PropTypes.func.isRequired,
    removefromFavs: PropTypes.func.isRequired,
    removeFromCart: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

function Favourites(props) {
    const {favourites, data} = useContext(Context)

    const favouriteObjects = data.filter(object => {
        for(let id of favourites){
            if (id === object.id){
                return true
            }
        }
    })

    const favouriteList = favouriteObjects.map(favItem => <GoodsItem
        key={favItem.id}
        addToFavs={props.addToFavs}
        removefromFavs={props.removefromFavs}
        removeFromCart={props.removeFromCart}
        addToCart={props.addToCart}
        name={favItem.name}
        year={favItem.year}
        location={favItem.location}
        manufacturer={favItem.manufacturer}
        snippet={favItem.snippet}
        flyingHours={favItem.flyingHours}
        price={favItem.price}
        src={favItem.src}
        id={favItem.id}
    />)


    return (
        <div>{favouriteList}</div>
    );
}

export default Favourites;