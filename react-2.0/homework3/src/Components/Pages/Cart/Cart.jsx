import React, {useContext} from 'react';
import './Cart.scss'
import GoodsItem from "../GoodsList/GoodsItem/GoodsItem";
import {Context} from "../../../Context";
import PropTypes from 'prop-types'

Cart.propTypes = {
    addToFavs: PropTypes.func.isRequired,
    removefromFavs: PropTypes.func.isRequired,
    removeFromCart: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

function Cart(props) {

    const {cart, data} = useContext(Context);

    const cartObjects = data.filter(object => {
        for (let id of cart) {
            if (id === object.id) {
                return true
            }
        }
    })
    console.log(props)

    const cartItems = cartObjects.map(cartItem => <GoodsItem key={cartItem.id}
                                                             addToFavs={props.addToFavs}
                                                             removefromFavs={props.removefromFavs}
                                                             addToCart={props.addToCart}
                                                             removeFromCart={props.removeFromCart}
                                                             name={cartItem.name}
                                                             year={cartItem.year}
                                                             location={cartItem.location}
                                                             manufacturer={cartItem.manufacturer}
                                                             snippet={cartItem.snippet}
                                                             flyingHours={cartItem.flyingHours}
                                                             price={cartItem.price}
                                                             src={cartItem.src}
                                                             id={cartItem.id}
    />)
    return (
        <div>
            <div className="cart-items">
                {cartItems}
            </div>
        </div>
    );
}

export default Cart;
