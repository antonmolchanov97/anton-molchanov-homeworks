import React,{useEffect,useState,useContext} from "react";
import Button from "../../../Button/Button";
import Modal from "../../../Modal/Modal";
import './GoodItem.scss'

import starempty from './icons/star-empty.svg'
import starfilled from './icons/star-full.svg'
import calendar from './icons/calendar.svg'
import location from './icons/location.svg'
import time from './icons/time.svg'
import {Context} from "../../../../Context";

import PropTypes from 'prop-types'

GoodsItem.propTypes = {
    addToFavs: PropTypes.func.isRequired,
    removefromFavs: PropTypes.func.isRequired,
    removeFromCart: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    year: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    manufacturer: PropTypes.string.isRequired,
    snippet: PropTypes.string.isRequired,
    flyingHours: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([PropTypes.string,PropTypes.number]),
}


function GoodsItem(props) {
    let {
        name,
        year,
        location,
        manufacturer,
        snippet,
        flyingHours,
        price,
        src,
        id
    } = props

    id = id.toString()

    const {cart,favourites} = useContext(Context)

    const [isAddedToFavs, setIsAddedToFavs] = useState(false)
    const [isModalOpen, setIsModalOpen] = useState(false)

    const [isInCart,setIsInCart] = useState(false)

    useEffect(()=>{
        const itemIsIncart = cart.some(cartID => cartID === id)
            setIsInCart(itemIsIncart)
    },[cart])

    useEffect(() => {
        favourites.includes(id) && setIsAddedToFavs(true)
    },[])

    const toggleFavsState = () => {
        if (isAddedToFavs) {
            props.removefromFavs(id)
        } else {
            props.addToFavs(id)
        }
        toggleDataInLocalStorage(id)
        setIsAddedToFavs(!isAddedToFavs)
    }

    const toggleDataInLocalStorage = (id) => {
        const rawDataFromLocalStorage = localStorage.getItem('favourites');
        let dataFromLocalStorage = JSON.parse(rawDataFromLocalStorage);
        if (!dataFromLocalStorage) {
            localStorage.setItem('favourites', JSON.stringify([id]))
            return
        }

        const idIsInArray = dataFromLocalStorage.find(item => item === id)
        if (idIsInArray) {
            dataFromLocalStorage = dataFromLocalStorage.filter(item => item !== id)
            localStorage.setItem('favourites', JSON.stringify(dataFromLocalStorage))
            return;
        }
        dataFromLocalStorage.push(id)
        localStorage.setItem('favourites', JSON.stringify(dataFromLocalStorage))
    }

    const toggleModal = () => {
        setIsModalOpen(!isModalOpen)
    }

    const addToCart = () => {
        props.addToCart(id)
        toggleModal()
    }

    const removeFromCart = () => {
        props.removeFromCart(id)
        toggleModal()
    }


    const addToCartButton = <Button
        backgroundColor='#6699ff'
        text='In den Warenkorb'
        onClick={toggleModal}/>

    const removeFromCartButton = <Button
        backgroundColor='#6699ff'
        text='Aus dem Warenkorb loeschen'
        onClick={toggleModal}/>

    const modalAddToCart = <Modal
        onClick={toggleModal}
        header='Do you want to add this item to cart?'
        text={name}
        subtext={price}
        closeButton={true}
        id="firstModal"
        cssClass='modal-wrapper-first modal-wrapper'
        actions={
            [
                <Button
                    backgroundColor='#999999'
                    text='Ok'
                    onClick={addToCart}
                />,
                <Button
                    backgroundColor='#999999'
                    text='Cancel'
                    onClick={toggleModal}
                />
            ]}
    />

    const modalRemoveFromCart = <Modal
        onClick={toggleModal}
        header='Do you want to remove this item from cart?'
        text={name}
        subtext={price}
        closeButton={true}
        id="firstModal"
        cssClass='modal-wrapper-first modal-wrapper'
        actions={
            [
                <Button
                    backgroundColor='#999999'
                    text='Ok'
                    onClick={()=>removeFromCart()}
                />,
                <Button
                    backgroundColor='#999999'
                    text='Cancel'
                    onClick={toggleModal}
                />
            ]}
    />

    let modal;
    if (isInCart){
        modal = modalRemoveFromCart
    } else {
        modal = modalAddToCart
    }


    return (
        <div className='goods-item'>
            <img className='item-img' src={src} alt=""/>
            <div className='item-info'>

                <div className="item-heading">
                    <div className="item-title-block">
                        <p className='item-title'>{name}</p>
                        <p className='item-subtitle'>{manufacturer}</p>
                    </div>
                    <div className="item-cart-fav">
                        <div className='favIcon-wrapper' onClick={toggleFavsState}>
                            <img className='favIcon' src={isAddedToFavs ? starfilled : starempty} alt=""/>
                        </div>
                        <div className='button-wrapper'>
                            {isInCart ? removeFromCartButton : addToCartButton}
                        </div>
                    </div>
                </div>

                <p className="item-price">{price}</p>

                <p className="item-snippet">{snippet}</p>

                <div className="item-addInfo">
                    <div className="addInfo">
                        <img className="addInfo-icon" src={calendar} alt=""/>
                        {year}
                    </div>
                    <div className="addInfo">
                        <img className="addInfo-icon" src={location} alt=""/>
                        {location}
                    </div>
                    <div className="addInfo">
                        <img className="addInfo-icon" src={time} alt=""/>
                        {flyingHours}
                    </div>
                </div>
            </div>
            {isModalOpen &&
            modal
            }
        </div>
    );
}
export default GoodsItem;

