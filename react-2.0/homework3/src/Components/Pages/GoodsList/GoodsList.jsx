import React, {useContext} from 'react';
import GoodsItem from "./GoodsItem/GoodsItem";
import './Goodslist.scss'
import {Context} from "../../../Context";
import PropTypes from 'prop-types'

GoodsList.propTypes = {
    addToFavs: PropTypes.func.isRequired,
    removefromFavs: PropTypes.func.isRequired,
    removeFromCart: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
}

function GoodsList(props) {

    const {data} = useContext(Context)

    const goodsList = data.map(goodItem => <GoodsItem
        key={goodItem.id}
        addToFavs={props.addToFavs}
        removefromFavs={props.removefromFavs}
        removeFromCart={props.removeFromCart}
        addToCart={props.addToCart}
        name={goodItem.name}
        year={goodItem.year}
        location={goodItem.location}
        manufacturer={goodItem.manufacturer}
        snippet={goodItem.snippet}
        flyingHours={goodItem.flyingHours}
        price={goodItem.price}
        src={goodItem.src}
        id={goodItem.id}

    />)

    return (
        <div className="goodslist">
            {goodsList}
        </div>
    );
}

export default GoodsList;
