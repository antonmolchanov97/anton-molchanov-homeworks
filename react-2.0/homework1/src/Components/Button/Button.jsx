import React from "react";
import './Button.scss'


export default class extends React.Component{
    render() {
        let button = this.props
        const {onClick, backgroundColor, text} = button
        return(
            <button
                className="btn"
                    style={{backgroundColor: backgroundColor}}
                    onClick={(event)=>onClick(event)}>
                {text}
            </button>
        )
    }
}