import React from "react";
import './App.css';
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal";

export default class App extends React.Component {
    state = {
        isModalOpen: false,
    }

    toggleModal() {
        this.setState({isModalOpen: !this.state.isModalOpen})
    }

    render() {
        const {isModalOpen} = this.state

        return (
            <div className="App">
                <div className='button-wrapper'>
                    <Button
                        backgroundColor='#6699ff'
                        text='Open first modal'
                        onClick={() => this.toggleModal()}/>
                    <Button
                        backgroundColor='#008ae6'
                        text='Open second modal'
                        onClick={() => this.toggleModal()}
                    />
                </div>
                {isModalOpen &&
                <Modal
                    target={() => this.target()}
                    onClick={() => this.toggleModal()}
                    header='Do you want to delete this file?'
                    text='Once you delete this file, it won`t be possible to undo this action.'
                    subtext='Are you sure you want to delete it?'
                    closeButton={true}
                    id="firstModal"
                    cssClass='modal-wrapper-first modal-wrapper'
                    actions={
                        [
                            <Button
                            backgroundColor='#999999'
                            text='Ok'
                            onClick={() => this.toggleModal()}
                            />,
                            <Button
                                backgroundColor='#999999'
                                text='Cancel'
                                onClick={() => this.toggleModal()}
                            />

                        ]}
                />
                }
                {isModalOpen &&
                <Modal
                    onClick={() => this.toggleModal()}
                    header='Do you want to rename this file?'
                    text='Renaming your file is reversible action'
                    subtext='Are you sure you want to rename it?'
                    closeButton={true}
                    id="secondModal"
                    cssClass='modal-wrapper-second modal-wrapper'
                    actions={
                        [
                            <Button
                                backgroundColor='#999999'
                                text='Ok'
                                onClick={() => this.toggleModal()}
                            />,
                            <Button
                                backgroundColor='#999999'
                                text='Cancel'
                                onClick={() => this.toggleModal()}
                            />

                        ]}
                />
                }


            </div>
        );
    }
}

