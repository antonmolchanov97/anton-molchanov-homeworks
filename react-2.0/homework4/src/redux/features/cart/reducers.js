import types from "./types";

const cartReducers = (state = [], action) => {
    const {type,payload} = action;
    const {ADDED_TO_CART,REMOVED_FROM_CART,GOT_CART_DATA_FROM_LS} = types

    switch (type){
        case ADDED_TO_CART:
            return payload
        case REMOVED_FROM_CART:
            return payload
        case GOT_CART_DATA_FROM_LS:
            return payload
        default:
            return state
    }
}

export default {
    cart: cartReducers
}