const GOT_CART_DATA_FROM_LS = "cart/gotCartDataFromLS";
const ADDED_TO_CART = "cart/addedToCart";
const REMOVED_FROM_CART = "cart/removedFromCart";

export default {
    GOT_CART_DATA_FROM_LS,
    ADDED_TO_CART,
    REMOVED_FROM_CART
}