import actions from "./actions";

const getCartFromLS = () => (dispatch) => {
    let localStorageData = JSON.parse(localStorage.getItem("cart"));
    if (!localStorageData) {
        return
    } else {
        dispatch(actions.gotCartDataFromLs(localStorageData))
    }
}

const addToCart = (id) => (dispatch,getState) => {
    const cart = getState().cart;
    const updatedCart = [...cart, id];
    localStorage.setItem("cart", JSON.stringify(updatedCart))
    dispatch(actions.addToCart(updatedCart))
}

const removeFromCart = (id) => (dispatch, getState) => {
    const cart = getState().cart;
    const updatedCart = cart.filter(cartID => cartID !== id)
    localStorage.setItem("cart", JSON.stringify(updatedCart))
    dispatch(actions.removeFromCart(updatedCart))
}


export default {
    addToCart,
    removeFromCart,
    getCartFromLS
}

