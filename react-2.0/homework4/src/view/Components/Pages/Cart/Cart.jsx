import React, {useContext} from 'react';
import './Cart.scss'
import GoodsItem from "../GoodsList/GoodsItem/GoodsItem";
import {Context} from "../../../../Context";
import PropTypes from 'prop-types'
import {useSelector} from "react-redux";
import {goodsSelectors} from "../../../../redux/features/goods/index";
import {cartSelectors} from "../../../../redux/features/cart/index";


function Cart(props) {

    const cart = useSelector(cartSelectors.cart)
    const goods = useSelector(goodsSelectors.goods)

    const cartObjects = goods.filter(object => {
        for (let id of cart) {
            if (id === object.id) {
                return true
            }
        }
    })
    console.log(props)

    const cartItems = cartObjects.map(cartItem => <GoodsItem key={cartItem.id}
                                                             name={cartItem.name}
                                                             year={cartItem.year}
                                                             location={cartItem.location}
                                                             manufacturer={cartItem.manufacturer}
                                                             snippet={cartItem.snippet}
                                                             flyingHours={cartItem.flyingHours}
                                                             price={cartItem.price}
                                                             src={cartItem.src}
                                                             id={cartItem.id}
    />)
    return (
        <div>
            <div className="cart-items">
                {cartItems}
            </div>
        </div>
    );
}

export default Cart;
